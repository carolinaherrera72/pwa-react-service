import React, { useState } from 'react';
import axios from 'axios';

const DataFetcher = () => {
    const [inputValue, setInputValue] = useState('');
    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const ruta = 'https://api.mercadolibre.com/sites/MLA/search?q=';

    const getData = async () => {
        setLoading(true);
        try {
            const response = await axios.get(ruta + inputValue);
            setData(response.data.results);
            setError(null);
        } catch (err) {
            setError(err);
            setData([]);
        } finally {
            setLoading(false);
        }
    };

    const handleInputChange = (event) => {
        setInputValue(event.target.value);
    };

    return (
        <div>
            <input
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                placeholder="Ingrese nombre"
            />
            <button onClick={getData}>Buscar</button>
            {loading && <p>Loading...</p>}
            {error && <p>Error: {error.message}</p>}
            <div className="productos">
                {data.map(item => (
                    <a key={item.id} className="modal-card" href={item.permalink} target="_blank" rel="noopener noreferrer">
                        <span className="h3">{item.title}</span>
                        <img src={item.thumbnail} aria-hidden="true" alt={item.title} />
                        <span className="price">Precio: ${item.price}</span>
                    </a>
                ))}
            </div>
        </div>
    );
};

export default DataFetcher;