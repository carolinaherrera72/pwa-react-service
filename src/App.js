import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import DataFetcher from './components/DataFetcher';

function App() {
    return (
        <Router>
            <div className="App">
                <Routes>
                    <Route path="/" element={<HomePage />} />            
                </Routes>
            </div>
        </Router>
    );
}
const HomePage = () => (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <div className="App">
                            <h1>Mercado Libre Productos</h1>
                            <DataFetcher/>
                        </div>
                    </div>
                </section>
                       <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
);
export default App;
